﻿
using BattleshipModel;
using BattleshipModel.Interfaces;
using BattleshipModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleshipApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IGame game = new GameConsoleApp();
            game.CreatePlayers();
            game.SetPlayersShips();
            game.ShowGrids();

            while(!game.IsFinished)
            {
                game.PlayRound();
            }

            game.ShowGameResult();

        }


    }
}
