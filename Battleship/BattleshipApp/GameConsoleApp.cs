﻿using BattleshipModel;
using BattleshipModel.Interfaces;
using BattleshipModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleshipApp
{
    public class GameConsoleApp : IGame
    {
        public IPlayer Player1 { get; set; }
        public IPlayer Player2 { get; set; }
        public bool IsFinished
        {
            get
            {
                return Player1.HasLost || Player2.HasLost;
            }
        }

        public void ShowGrids()
        {
            ShowPlayerGrids(Player1);
            ShowPlayerGrids(Player2);
        }

        public void ShowPlayerGrids(IPlayer player)
        {
            Console.WriteLine(player.Name);
            Console.WriteLine("Player grid:                                                 Hits grid:");
            for (int row = 1; row <= 10; row++)
            {
                for (int column = 1; column <= 10; column++)
                {
                    Console.Write($" | {player.PlayerGrid.Squares.First(p => p.Location.Row == row && p.Location.Column == column).State.ToString().Substring(0, 1)}");
                    if (column == 10)
                    {
                        Console.Write(" |");
                    }
                }
                Console.Write("                 ");
                for (int hitsCol = 1; hitsCol <= 10; hitsCol++)
                {
                    Console.Write($" | {player.HitsGrid.Squares.First(p => p.Location.Row == row && p.Location.Column == hitsCol).State.ToString().Substring(0, 1)}");
                    if (hitsCol == 10)
                    {
                        Console.Write(" |");
                    }
                }
                Console.WriteLine(Environment.NewLine);
            }
            Console.WriteLine(Environment.NewLine);
        }

        public void CreatePlayers()
        {
            Player1 = CreatePlayer();
            Player2 = CreatePlayer();
        }

        public void PlayRound()
        {
            var location = Player1.PrepareShotLocation();
            var result = Player2.Shot(location);
            Player1.UpdateHitsGrid(location, result);


            location = Player2.PrepareShotLocation();
            result = Player1.Shot(location);
            Player2.UpdateHitsGrid(location, result);
        }

        private IPlayer CreatePlayer()
        {
            Console.WriteLine("Player name:");
            var nameP1 = Console.ReadLine();
            IPlayer player = new Player(nameP1);
            return player;
        }

        public void ShowGameResult()
        {
            string wonPlayerName = Player1.HasLost ? Player2.Name : Player1.Name;
            Console.WriteLine($"{wonPlayerName} has won the game!");

            ShowGrids();
            Console.ReadKey();
        }
    }
}
