﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipModel
{
    public enum SquareState
    {
        Empty,
        Ship,
        Hit,
        Miss
    }
}
