﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipModel
{
    public enum ShotResult
    {
        Hit,
        Miss,
        HitAndSink
    }
}
