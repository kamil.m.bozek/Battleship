﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipModel
{
    public interface ILocation
    {
        int Row { get; }
        int Column { get; }
    }
}
