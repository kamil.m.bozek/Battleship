﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipModel.Interfaces
{
    public interface IGame
    {
        IPlayer Player1 { get; set; }
        IPlayer Player2 { get; set; }
        bool IsFinished { get; }
        void ShowGrids();
        void CreatePlayers();
        void SetPlayersShips()
        {
            Player1.SetShips();
            Player2.SetShips();
        }

        void PlayRound();
        void ShowGameResult();
    }
}
