﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipModel
{
    public interface ISquare
    {
        ILocation Location { get; }
        SquareState State { get; set; }
    }
}
