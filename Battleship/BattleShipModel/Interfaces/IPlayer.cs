﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleshipModel
{
    public interface IPlayer
    {
        string Name { get; }
        IGrid PlayerGrid { get; }
        IGrid HitsGrid { get; }
        List<IShip> Ships { get; }
        ISquare LastHitSquare { get; set; }
        bool HasLost
        {
            get
            {
                return Ships.All(p => p.IsSunk);
            }
        }

        ShotResult Shot(ILocation location)
        {
            ShotResult result = ShotResult.Miss;
            var square = PlayerGrid.Squares.First(p => p.Location.Column == location.Column && p.Location.Row == location.Row);

            if (square.State == SquareState.Ship)
            {
                var ship = Ships.First(p => p.Squares.Any(s => s.Location.Column == location.Column && s.Location.Row == location.Row));
                ship.Hits++;
                result = ship.IsSunk ? ShotResult.HitAndSink : ShotResult.Hit;
            }

            return result;
        }

        ILocation PrepareShotLocation()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            List<ILocation> squares = new List<ILocation>();
            if (LastHitSquare != null)
            {
                squares = GetHitsNeighbors();
            }

            if (squares.Count == 0)
            {
                squares = GetShotCandidateSquares();
            }

            var index = random.Next(squares.Count);
            return squares[index];
        }

        List<ILocation> GetShotCandidateSquares()
        {
            return HitsGrid.Squares.Where(x => x.State == SquareState.Empty)
                         .Select(x => x.Location)
                         .ToList();
        }

        List<ILocation> GetHitsNeighbors()
        {
            List<ISquare> squares = new List<ISquare>();
            HitsGrid.Squares.Where(x => x.State == SquareState.Hit)
                     .ToList().ForEach(x => squares.AddRange(HitsGrid.GetNeightbors(x)));

            return squares.Where(p => p.State == SquareState.Empty).Distinct().Select(s => s.Location).ToList();
        }

        void SetShips()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            foreach (var ship in Ships)
            {
                bool isSetOnGrid = false;
                do
                {
                    bool horizontalOrientation = random.Next(2) == 0;

                    var columnStart = random.Next(1, 11);
                    var rowStart = random.Next(1, 11);
                    var columnEnd = horizontalOrientation ? columnStart + ship.Size : columnStart;
                    var rowEnd = horizontalOrientation ? rowStart : rowStart + ship.Size;

                    var shipSquares = PlayerGrid.Squares.Where(p => p.Location.Column >= columnStart
                                                 && p.Location.Column <= columnEnd
                                                 && p.Location.Row >= rowStart
                                                 && p.Location.Row <= rowEnd);

                    if (rowEnd > 10 || columnEnd > 10 || shipSquares.Any(p => p.State != SquareState.Empty))
                    {
                        continue;
                    }

                    ship.Squares = shipSquares.ToList();
                    ship.Squares.ForEach(p => p.State = SquareState.Ship);
                    isSetOnGrid = true;
                }
                while (!isSetOnGrid);
            }
        }

        void UpdateHitsGrid(ILocation location, ShotResult shotResult)
        {
            var square = HitsGrid.Squares.First(p => p.Location.Row == location.Row && p.Location.Column == location.Column);
            square.State = shotResult == ShotResult.Miss ? SquareState.Miss : SquareState.Hit;
            if(shotResult == ShotResult.Hit)
            {
                LastHitSquare = square;
            }
            else if(shotResult == ShotResult.HitAndSink)
            {
                LastHitSquare = null;
            }
        }
    }
}
