﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleshipModel
{
    public interface IShip
    {
        int Size { get; }
        int Hits { get; set; }
        List<ISquare> Squares { get; set; }
        bool IsSunk
        {
            get
            {
                return Hits >= Size;
            }
        }
    }
}
