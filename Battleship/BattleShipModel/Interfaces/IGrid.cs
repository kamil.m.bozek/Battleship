﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipModel
{
    public interface IGrid
    {
        List<ISquare> Squares { get; }
        int GridSize { get; }
        void CreateGrid();
        List<ISquare> GetNeightbors(ISquare square);
    }
}
