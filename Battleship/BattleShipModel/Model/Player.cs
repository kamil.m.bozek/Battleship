﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleshipModel.Model
{
    public class Player : IPlayer
    {
        public string Name { get; set; }
        public IGrid PlayerGrid { get; set; }
        public IGrid HitsGrid { get; set; }
        public List<IShip> Ships { get; set; }
        public ISquare LastHitSquare { get; set; }

        public Player(string name)
        {
            Name = name;

            PlayerGrid = new Grid();
            PlayerGrid.CreateGrid();

            HitsGrid = new Grid();
            HitsGrid.CreateGrid();

            Ships = new List<IShip>();           
            Ships.Add(new Ship
            {
                Size = 2
            });
            Ships.Add(new Ship
            {
                Size = 3
            });
            Ships.Add(new Ship
            {
                Size = 3
            });
            Ships.Add(new Ship
            {
                Size = 4
            });
            Ships.Add(new Ship
            {
                Size = 5
            }); 
        }
    }
}
