﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipModel.Model
{
    public class Ship : IShip
    {
        public int Size { get; set; }

        public int Hits { get; set; }

        public List<ISquare> Squares { get; set; }
    }
}
