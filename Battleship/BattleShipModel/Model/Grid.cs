﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleshipModel
{
    public class Grid : IGrid
    {
        public List<ISquare> Squares { get; set; }

        public int GridSize 
        { 
            get
            {
                return 10;
            }
        }

        public void CreateGrid()
        { 
            Squares = new List<ISquare>();
            for (int i = 1; i <= GridSize; i++)
            {
                for (int j = 1; j <= GridSize; j++)
                {
                    Squares.Add(new Square(i, j));
                }
            }
        }

        public List<ISquare> GetNeightbors(ISquare square)
        {
            List<ISquare> neightbors = new List<ISquare>();
            if (square.Location.Column < GridSize)
            {
                neightbors.Add(Squares.First(p => p.Location.Column == square.Location.Column + 1 && p.Location.Row  == square.Location.Row));
            }
            if (square.Location.Column > 1)
            {
                neightbors.Add(Squares.First(p => p.Location.Column == square.Location.Column - 1 && p.Location.Row == square.Location.Row));
            }
            if (square.Location.Row < GridSize)
            {
                neightbors.Add(Squares.First(p => p.Location.Column == square.Location.Column && p.Location.Row == square.Location.Row + 1));
            }
            if (square.Location.Row > 1)
            {
                neightbors.Add(Squares.First(p => p.Location.Column == square.Location.Column && p.Location.Row == square.Location.Row - 1));
            }

            return neightbors;
        }
    }
}
