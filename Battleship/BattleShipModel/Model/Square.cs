﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipModel
{
    public class Square : ISquare
    {
        public ILocation Location { get; set; }

        public SquareState State { get; set; }
        public Square(int row, int column)
        {
            Location = new Location(row, column);
        }
    }
}
