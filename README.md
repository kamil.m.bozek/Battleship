# Battleship



## Information
Solution contains 2 project BattleshipApp and BattleshipModel. BattleshipApp is consoleApplication and has only 2 classes (Program.cs for run application and GameConsoleApp.cs with main functions for the game).
BattleshipModel is library with all interfaces and classes used for game (ex. grid, square).

## TODO
1. Algorithm for hits preparation should be improved.
2. Configuration for some game rules (ex. position of ship, number of ships) should be added.
3. Additional text information for all rounds of game should be created.
